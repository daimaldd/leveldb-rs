struct BoolFilter(isize);

impl From<isize> for BoolFilter {
    fn from(i: isize) -> Self {
       BoolFilter(i) 
    }
}

impl BoolFilter {
    const NAME: &'static str = "eveldb.BuiltinBloomFilter";
    
    pub(crate) fn contains(&self, filter: &[u8], key: &[u8]) -> bool {
        let n_bytes = filter.len() - 1;
        if n_bytes < 1 {
            return false;
        }

        let n_bits = (n_bytes * 8 ) as u32;
        
        // Use the  encoded k so that we can read filters gnerated by
        // bloom filters created using different parameters.
        let k = filter[n_bytes];
        
        if k > 30 {
            // Reserved for potentially new encodings for short bloom filters.
            // Consider it a match.
            return true;
        }

        let mut kh = Self::bloom_hash(key);
        let delta = (kh >> 17) | (kh << 15); // Rotate right 17 bits
        for j in 0..k {
            let bitpos = kh % n_bits;
            if (filter[bitpos as usize /8] as u32) & (1 << (bitpos % 8) ) == 0 {
                return false
            }

            kh += delta;
        }
    true 
    }

    fn bloom_hash(key: &[u8]) -> u32 {
        todo!()
    }
}

struct BloomFilterGenerator {
    n: isize,
    k: u8,
    key_hashs: Vec<u32>,
}

impl BloomFilterGenerator {
    fn add(&mut self, key: Vec<u8>) {
        // Use double-hashing to generate a sequence of hash values.
        // See analysis in [Kirsh, Mitzenmacher 2006]
        self.key_hashs.push(BoolFilter::bloom_hash(&key))
    } 

    fn generate(&mut self, buf: &mut [u8]) {
        // Compute bloom fiter size (in both bits and bytes)
        let  mut n_bits = self.key_hashs.len() * self.n as usize;
    

        // For small n, we can see a very high false positive rate. Fix it
        // by enforcing a minimum bloom filter length.
        if n_bits < 64 {
            n_bits = 64;
        }

        n_bits = (n_bits + 7) / 8;
        n_bits = n_bits * 8;

        // todo!
        todo!()

    }
}
